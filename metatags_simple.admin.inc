<?php
// $Id$

/**
 * @file
 * Metatags Simple - Administration Pages & Functions
 *
 * @author
 * Marco Vito Moscaritolo <mavimo@gmail.com>
 */

/**
 * Generate page with complete listo of metatag with sorting and paging capabilities.
 */
function metatags_simple_admin_page() {
  /**
  * @todo: converto to local action
   */
  $html = l(t('Add'), 'admin/config/search/metatags-simple/add');

  // $form = drupal_get_form('metatags_simple_custom_title_form');
  // $html .= drupal_render($form);

  $header = array(
    array(
      'data' => t('Path'),
      'field' => 'path',
      'sort' => 'asc',
    ),
    array(
      'data' => t('Name'),
      'field' => 'name',
    ),
    array(
      'data' => t('Value'),
      'field' => 'value',
    ),
    '',
    '',
  );  
  
  $query = db_select('metatags_simple', 'ms')->extend('TableSort')->extend('PagerDefault');
  $query->fields('ms', array('mid', 'path', 'name', 'value'));
  $query->orderByHeader($header);
  $query->limit(25);
  $result = $query->execute();

  $rows = array();
  foreach ($result as $data) {
    $rows[] = array(
      t('@path', array('@path' => check_plain($data->path))),
      $data->name,
      t('@content', array('@content' => check_plain($data->value))),
      l(t('edit'),   "admin/config/search/metatags-simple/{$data->mid}/edit"),
      l(t('delete'), "admin/config/search/metatags-simple/{$data->mid}/delete")
    );
  }

  if (empty($rows)) {
    $empty_message = t('No custom metatags available.');
    $rows[] = array(array('data' => $empty_message), '');
  }

  $html .= theme('table', array('header' => $header, 'rows' => $rows));
  $html .= theme('pager', array('tags' => NULL, 'element' => 0));

  return $html;
}

/**
 * Form constructor for the metatags deleting
 *
 * @param $metatag
 *   The metatag to edit
 *
 * @see metatags_simple_metatag_delete_form_submit()
 *
 * @ingroup forms
 */
function metatags_simple_metatag_delete_form($form, $form_state, $metatag) {
  $form['#metatag'] = $metatag;

  // Always provide entity id in the same form key as in the entity edit form.
  $form['mid'] = array(
    '#type' => 'value',
    '#value' => $metatag->mid,
  );

  return confirm_form(
    $form, 
    t('Are you sure you want to delete the metatag?'), 
    'admin/config/search/metatags-simple', 
    t('This action cannot be undone.'), 
    t('Delete'), 
    t('Cancel'), 
    'metatags_simple_metatags_delete'
  );
}

/**
 * Form submission handler for metatags_simple_metatag_delete_form().
 *
 * @see metatags_simple_metatag_delete_form()
 */
function metatags_simple_metatag_delete_form_submit($form, &$form_state) {
  $mid = $form_state['values']['mid'];

  metatags_simple_metatags_delete($mid);
  $form_state['redirect'] = 'admin/config/search/metatags-simple';
}

/**
 * Form constructor for the metatags editing
 *
 * @param $metatag
 *   The metatag to edit
 *
 * @see metatags_simple_metatag_edit_form_submit()
 *
 * @ingroup forms
 */
function metatags_simple_metatag_edit_form($form, $form_state, $metatag = NULL) {
  $form = array();

  $mode = (is_null($metatag)) ? 'create' : 'update';

  $form['mode'] = array(
    '#type' => 'hidden',
    '#value' => $mode,
  );

  if ($mode == 'update') {
    $form['mid'] = array(
      '#type' => 'hidden',
      '#value' => (isset($metatag->mid)) ? $metatag->mid : NULL,
    );
  }

  $form['description'] = array(
    '#type' => 'markup',
    '#value' => '<p style="margin-top: 10px;">' . t('You may use this form to create custom &lt;meta&gt; tag content. If you wish to edit or delete an existing metatag please find it in the table below.') . '</p>',
  );

  $form['path'] = array(
    '#type' => 'textfield',
    '#length' => 30,
    '#maxlength' => 200,
    '#title' => 'Path',
    '#description' => t('Please enter the path/url of the page you would like to apply a new metatag to.'),
    '#required' => TRUE,
    '#disabled' => FALSE,
    '#default_value' => (!is_null($metatag->path)) ? $metatag->path : NULL,
  );

  $form['name'] = array(
    '#type' => 'select', 
    '#title' => t('Meta tag name'), 
    '#description' => t('Select the type of meta tags do you would to set on specified path.'),
    '#default_value' => (!is_null($metatag->name)) ? $metatag->name : NULL,
    '#options' => metatags_simple_get_names(), 
  );

  $form['value'] = array(
    '#type' => 'textfield',
    '#length' => 200,
    '#maxlength' => 250,
    '#title' => 'Value',
    '#description' => t('Please enter value to apply a new metatag to.'),
    '#required' => TRUE,
    '#disabled' => FALSE,
    '#default_value' => (!is_null($metatag->value)) ? $metatag->value : NULL,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

/**
 * Form submission handler for metatags_simple_metatag_edit_form().
 *
 * @see metatags_simple_metatag_edit_form()
 */
function metatags_simple_metatag_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  if($values['mode'] == 'create') {
    $message = t('Meta tag created');
    $mid = metatags_simple_metatags_create($values['path'], $values['name'], $values['value']);
    $form_state['redirect'] = "admin/config/search/metatags-simple/{$mid}/edit"
  } else {
    $message = t('Meta tag updated');
    metatags_simple_metatags_update($values['mid'], $values['path'], $values['name'], $values['value']);
  }

  drupal_set_message($message);
}